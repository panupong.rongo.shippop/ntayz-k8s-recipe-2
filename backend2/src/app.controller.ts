import { Controller, Get } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PinoLogger, InjectPinoLogger } from 'nestjs-pino'
import { AppService } from './app.service'

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private configService: ConfigService,
    @InjectPinoLogger(AppController.name)
    private readonly logger: PinoLogger,
  ) {}

  @Get()
  getHello(): string {
    // this.logger.trace({ foo: 'bar' }, 'baz %s', 'qux')
    this.logger.error('foo %s %o', 'bar', { baz: 'qux' })
    // this.logger.info('foo')
    // console.info(`appMode`, appMode)
    const nodeEnv = this.configService.get<string>('nodeEnv')

    // console.error(`Read`, nodeEnv)

    return this.appService.getHello()
  }

  @Get('sample')
  getSample(): { message: string; reqDate: string } {
    const d = new Date()
    return { message: 'OK', reqDate: d.toISOString() }
  }
}
