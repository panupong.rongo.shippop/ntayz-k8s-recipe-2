module.exports = {
  serverRuntimeConfig: {
    backend1: {
      url: process.env.BACKEND1_SERVER_URL,
    },
    backend2: {
      url: process.env.BACKEND2_SERVER_URL,
    },
  },
  publicRuntimeConfig: {
    backend1: {
      url: process.env.BACKEND1_PUBLIC_URL,
    },
    backend2: {
      url: process.env.BACKEND2_PUBLIC_URL,
    },
  },
}
