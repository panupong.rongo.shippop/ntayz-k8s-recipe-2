import React from 'react'
import Link from 'next/link'
import getConfig from 'next/config'

import useSWR from 'swr'
import axios from 'axios'

import Layout from '../components/Layout'

const fetcher = (url: string) => axios.get(url).then((res) => res.data)
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

function IndexPage({ ssrReqDate1 = '-', ssrReqDate2 = '-' }) {
  const { backend1, backend2 } = publicRuntimeConfig

  const { data: data1 } = useSWR(`${backend1.url}sample`, fetcher, {
    refreshInterval: 60000,
  })
  let reqDate1 = ``
  if (data1) {
    const { reqDate }: { reqDate: string } = data1
    reqDate1 = reqDate
  }

  const { data: data2 } = useSWR(`${backend2.url}sample`, fetcher, {
    refreshInterval: 60000,
  })
  let reqDate2 = ``
  if (data2) {
    const { reqDate }: { reqDate: string } = data1
    reqDate2 = reqDate
  }

  return (
    <Layout title="Home | Next.js + TypeScript Example">
      <h1>Hello Next.js 👋</h1>
      <p>
        <Link href="/about">About</Link>
      </p>
      <section>
        <h2> Connect to Backend1</h2>
        <h3> SSR Request </h3>
        <p>Requset @ {ssrReqDate1 || '-'}</p>
        <h3> SPA Request </h3>
        <p>Requset @ {reqDate1 || '-'}</p>
        <h2> Connect to Backend2</h2>
        <h3> SSR Request </h3>
        <p>Requset @ {ssrReqDate2 || '-'}</p>
        <h3> SPA Request </h3>
        <p>Requset @ {reqDate2 || '-'}</p>
      </section>
    </Layout>
  )
}

// // This gets called on every request
export async function getServerSideProps() {
  const { backend1, backend2 } = serverRuntimeConfig

  const result1 = await axios.get(`${backend1.url}sample`)
  const { reqDate: ssrReqDate1 } = result1.data

  const result2 = await axios.get(`${backend2.url}sample`)
  const { reqDate: ssrReqDate2 } = result2.data

  // Pass data to the page via props
  return { props: { ssrReqDate1, ssrReqDate2 } }
}

export default IndexPage
