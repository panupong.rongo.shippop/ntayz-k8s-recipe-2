# ntayz-k8s-recipe-2

เป็นการฝึกสร้าง microservices แบบง่ายๆ Develope โดยใช้ภาษา javascript (ts) โดย Front-End เป็น Nextjs และ Backend เป็น Nestjs

## Pre-Require

- basic javascript
- basic docker & docker-compose
- basic K8S

## Pre-Setup

1. สร้าง folder `.vscode ที่ root`

2. สร้าง file `.vscode/extenstion.json`

```json
{
  "recommendations": [
    "coenraads.bracket-pair-colorizer-2",
    "ms-azuretools.vscode-docker",
    "mikestead.dotenv",
    "dbaeumer.vscode-eslint",
    "eamodio.gitlens",
    "eg2.vscode-npm-script",
    "howardzuo.vscode-npm-dependency",
    "esbenp.prettier-vscode",
    "vscode-icons-team.vscode-icons",
    "jpoissonnier.vscode-styled-components",
    "ms-vsliveshare.vsliveshare",
    "ms-vscode.vscode-typescript-next"
  ],
  "unwantedRecommendations": []
}
```

3. สร้าง file `.vscodesettings.json`

```json
{
  "editor.tabSize": 2,
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    // For ESLint
    "source.fixAll.eslint": true,
    // For TSLint
    "source.fixAll.tslint": true,
    // For Stylelint
    "source.fixAll.stylelint": true
  },
  "files.eol": "\n",
  "files.watcherExclude": {
    "**/node_modules/**": true
  },
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "eslint.packageManager": "yarn",
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact"
  ],
  "eslint.workingDirectories": [{ "mode": "auto" }],
  "editor.suggestSelection": "first",
  "editor.snippetSuggestions": "top",
  "editor.scrollBeyondLastLine": false
}
```

## Outline

- set-up "FRONT-END" project
- set-up "BACK-END" 2 project
- setup 3 projects with docker-compose
- orchres all project with k8s

## set-up "FRONT-END" project

1. clone nextjs ที่เป็น typescript

```shell
yarn create next-app --example with-typescript frontend
```

2. เริ่มกำหนด eslint ให้ fontend

```json
{
    ...
    "lint": "next lint"
    ...
}
```

2.1 สังคำสัง `yarn lint`

```shell
yarn lint
```

2.2 ติดตั้ง dependency `airbnb eslint`

```shell
yarn add -D eslint eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks
```

2.3 ติดตั้่ง dependency `typescript airbnb eslint`

```shell
yarn add -D eslint-config-airbnb-typescript @typescript-eslint/eslint-plugin@^5.13.0 @typescript-eslint/parser@^5.0.0
```

2.4 ติดตั้่ง dependency `prettier`

```shell
yarn add -D prettier eslint-plugin-prettier eslint-config-prettier
```

2.6 ตั้่งค่าไฟล์ `.eslintrc.json`

```json
{
  "parser": "@typescript-eslint/parser",
  "plugins": ["@typescript-eslint", "react-hooks", "prettier"],
  "extends": [
    "airbnb",
    "airbnb/hooks",
    "airbnb-typescript",
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@next/next/recommended",
    "plugin:jsx-a11y/recommended",
    "prettier",
    "next/core-web-vitals"
  ],
  "env": {
    "node": true,
    "browser": true
  },
  "parserOptions": {
    "project": "./tsconfig.json",
    "jsx": true,
    "useJSXTextNode": true
  },
  "settings": {
    "import/resolver": {
      "babel-module": {}
    }
  },
  "rules": {
    "prettier/prettier": "warn",
    "camelcase": "off",
    "no-console": ["warn", { "allow": ["info", "warn", "error"] }],
    "no-unused-vars": "warn",
    "no-underscore-dangle": "off",
    "no-param-reassign": "off",
    "no-nested-ternary": "off",
    "no-alert": "off",
    "import/prefer-default-export": "off",
    "react/prop-types": "off",
    "react/jsx-one-expression-per-line": "off",
    "react/jsx-indent": "off",
    "react/jsx-curly-newline": "off",
    "react/jsx-props-no-spreading": "off",
    "react/jsx-wrap-multilines": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "eslint-disable-next-line": "off"
  }
}
```

2.5 ตั้่งค่าไฟล์ `.prettierrc.json`

```json
{
  "singleQuote": true,
  "trailingComma": "all",
  "semi": false,
  "arrowParens": "always"
}
```

## set-up "BACK-END" 2 project

clone `nestjs` ที่เป็น backend มา 2 ตัว
https://gitlab.com/panupong.rongo.shippop/ultima-nestjs

- ตัวที่ 1 run ที่ port 3001
- ตัวที่ 1 run ที่ port 3002

## Sync forntend-backend

1. สร้าง route "/sample" ที่ ./backend1/src/app.controller.ts

```typescript
@Get('sample')
  getSample(): { message: string; reqDate: string } {
    const d = new Date()
    return { message: 'OK', reqDate: d.toISOString() }
  }
```

2. ที่ folder `frontend` ลง dependency `swr axios`

- swr https://swr.vercel.app/
- axios https://github.com/axios/axios

```shell
yarn add axios swr
```

3. ต่อ api จาก Backend1 ทั้งแบบ SSR และ SPA

3.1 ต่อ api แบบ SSR ด้วย nextjs https://nextjs.org/docs/basic-features/pages#server-side-rendering

```typescript
// This gets called on every request
export async function getServerSideProps() {
  const result1 = await axios.get(`http://localhost:3001/sample`);
  const { reqDate: ssrReqDate1 } = result1.data;

  // Pass data to the page via props
  return { props: { ssrReqDate1 } };
}
```

3.2 ต่อ api แบบ spa ด้วน swr และ axios

```typescript
const fetcher1 = (url: string) => axios.get(url).then((res) => res.data)
function IndexPage({ ssrReqDate1 }) {
  const { data: data1 } = useSWR('http://localhost:3001/sample', fetcher1, {
    refreshInterval: 60000,
  })
...
Rest of code
...
}

}
```

4. สร้าง route "/sample" ที่ ./backend2/src/app.controller.ts และ ต่อ api ที่ frontend

5. สร้าง file `.env` ที่ ./frontend

```.env
BACKEND1_PUBLIC_URL=http://localhost:3001/
BACKEND1_SERVICE_URL=http://localhost:3001/
BACKEND2_PUBLIC_URL=http://localhost:3002/
BACKEND2_SERVICE_URL=http://localhost:3002/
```

6. สร้าง file `next.config.js` ที่ ./frontend

```javascript
module.exports = {
  serverRuntimeConfig: {
    backend1: {
      url: process.env.BACKEND1_SERVER_URL
    },
    backend2: {
      url: process.env.BACKEND2_SERVER_URL
    }
  },
  publicRuntimeConfig: {
    backend1: {
      url: process.env.BACKEND1_PUBLIC_URL
    },
    backend2: {
      url: process.env.BACKEND2_PUBLIC_URL
    }
  }
};
```

7. วิธีการ `env` ใน `nextjs`
   https://nextjs.org/docs/api-reference/next.config.js/runtime-configuration

## Setup Develope enviroment with docker and docker-compose

สร้าง folder `docker` ที่ root floder

### สร้าง docker env สำหรับ env `developement`

- สร้าง file `dockerfile.frontend.dev` ที่ `/docker`

```dockerfile
FROM node:14-alpine

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install

COPY ./ ./

EXPOSE 3000

CMD [ "yarn" , "run" , "dev" ]

```

- สร้าง file `docker-compose.dev.yaml` ที่ `/docker` และตังค่า services จาก Dockerfile.frontend.dev

```docker-compose
version: "3.8"
services:
  frontend:
    build:
      context: ../frontend/
      dockerfile: ../docker/Dockerfile.frontend.dev
    env_file:
      - ../frontend/.env
    volumes:
      - ../frontend/:/app
      - /app/node_modules
    ports:
      - "3000:3000"
```

- สร้าง file `dockerfile.nestjs.dev` ที่ `/docker`

```dockerfile
FROM node:14-alpine

WORKDIR /app

COPY package.json ./

RUN yarn install

COPY ./ ./

EXPOSE 3000

CMD [ "yarn" , "run" , "start:dev" ]
```

\*\*\* แก้ให้ backend1 รันที่ port 3000

- แก้ไข file `docker-compose.dev.yaml` ที่ `/docker` เพื่อเพิ่ม services `backend1`

```docker-compose
version: "3.8"
services:
  backend1:
    build:
      context: ../backend1/
      dockerfile: ../docker/Dockerfile.nestjs.dev
    env_file:
      - ../backend1/.env.local
    volumes:
      - ../backend1/:/app
      - /app/node_modules
    ports:
      - "3001:3000"
  frontend:
    build:
      context: ../frontend/
      dockerfile: ../docker/Dockerfile.frontend.dev
    env_file:
      - ../frontend/.env
    volumes:
      - ../frontend/:/app
      - /app/node_modules
    ports:
      - "3000:3000"
    depends_on:
      - backend1

```

- แก้ไขตัวแปร `BACKEND1_SERVER_URL`ที่ `/frontend/.env`

```
BACKEND1_SERVER_URL=http://backend1:3000/
```

\*\*\* ถ้าการ connection เป็นระบบ server จะใช้ชื่อ services ของ docker-compose แทนที่จะเป็น localhost

- แก้ไข file `docker-compose.dev.yaml` ที่ `/docker` เพื่อเพิ่ม services `backend2` และแก้ไขตัวแปร `BACKEND1_SERVER_URL`ที่ `/frontend/.env`

```docker-compose
version: "3.8"
services:
  backend1:
    build:
      context: ../backend1/
      dockerfile: ../docker/Dockerfile.nestjs.dev
    env_file:
      - ../backend1/.env.local
    volumes:
      - ../backend1/:/app
      - /app/node_modules
    ports:
      - "3001:3000"
  backend2:
    build:
      context: ../backend2/
      dockerfile: ../docker/Dockerfile.nestjs.dev
    env_file:
      - ../backend1/.env.local
    volumes:
      - ../backend2/:/app
      - /app/node_modules
    ports:
      - "3002:3000"
  frontend:
    build:
      context: ../frontend/
      dockerfile: ../docker/Dockerfile.frontend.dev
    env_file:
      - ../frontend/.env
    volumes:
      - ../frontend/:/app
      - /app/node_modules
    ports:
      - "3000:3000"
    depends_on:
      - backend1
      - backend2
```

`frontend/.env`

```
BACKEND1_SERVER_URL=http://backend2:3000/
```

## Setup Production enviroment with docker and docker-compose

- สร้าง dockerfile.prod สำหรับ `frontend` project

```dockerfile
# 1. Install dependencies only when needed
# 0. Install productuib dependencies only when needed
FROM node:16-alpine AS install_modules_stage
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --production



# 1. Install dependencies only when needed
FROM node:16-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

# 2. Rebuild the source code only when needed
FROM node:16-alpine AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .
# This will do the trick, use the corresponding env file for each environment.
COPY .env .env
RUN yarn build

# 3. Production image, copy all the files and run next
FROM node:16-alpine AS runner
WORKDIR /app

ENV NODE_ENV=production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/package.json ./package.json


# copy production deps
COPY --from=install_modules_stage /app/node_modules/ ./node_modules


# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/ ./.next


USER nextjs

EXPOSE 3000

ENV PORT 3000

CMD [ "yarn" , "run" , "start" ]
```

- สร้าง file `docker-compose.prod.yaml` ที่ `/docker` เพื่อเพิ่ม services `frontend`

```docker-compose
version: "3.8"
services:
  frontend:
    build:
      context: ../frontend/
      dockerfile: ../docker/Dockerfile.frontend.prod
    env_file:
      - ../frontend/.env
    ports:
      - "3000:3000"

```

- สร้าง dockerfile.prod สำหรับ `nestjs` project และสร้าง services `backend1 backend2` ใน `docker-compose.prod.yaml`

```dockerfile
########## install_modules_stage ##########
FROM node:16-alpine as install_modules_stage

# set working directory
WORKDIR /app/
# install production dependencies
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile --production

########## build_stage ##########

FROM install_modules_stage as build_stage

# install all dependencies
RUN yarn install --frozen-lockfile

# copy all files
COPY .eslintrc.js nest-cli.json tsconfig.json tsconfig.build.json /app/
COPY src/ /app/src/

# build the production app
RUN yarn build

########## final_stage ##########
FROM node:16-alpine as final_stage

# set working directory
WORKDIR /app/


# copy package.json file
COPY --from=build_stage /app/package.json /app/

# copy production deps
COPY --from=install_modules_stage /app/node_modules/ /app/node_modules/

# copy production app
COPY --from=build_stage /app/dist/ /app/dist/

EXPOSE 3000

CMD [ "yarn" , "run" , "start:prod" ]
```

## create `shell scripts` for better dev-exp

1. สร้าง folder `scripts`

2. สร้าง `dev-start.sh` สำหรับทำให้ docker-compose ของ dev enviroment เริื่มทำงาน

3. สร้าง `dev-build.sh` สำหรับบังคับให้มีการ build dockerfile ใหม่กรณีต้องการลง dependency ของ nodejs เพิ่ม

4. สร้าง `dev-down.sh` ปิด docker-compose ของ dev env

5. สร้าง `prod-start.sh` สำหรับทำให้ docker-compose ของ prod enviroment เริื่มทำงาน

6. สร้าง `prod-build.sh` สำหรับบังคับให้มีการ build dockerfile ใหม่กรณีต้องการลง dependency ของ nodejs เพิ่ม

7. สร้าง `prod-down.sh` ปิด docker-compose ของ prod env

## orchres all project with k8s

0. ลองสร้าง docker images ของ `frontend` project

```shell
docker build -f docker/Dockerfile.frontend.prod ./frontend/ -t ntayz-k8s-recipe-2/frontend:1.0.0
```

1. สร้าง namespace ของ k8s ชื่อ `ntayz-k8s-recipe-2`

`00-namespace.yaml`

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ntayz-k8s-recipe-2
  labels:
    name: ntayz-k8s-recipe-2
    module: Namespace
```

สร้าง Namespace

```shell
 kubectl apply -f ./k8s/00-namespace.yaml
```

คำสั่งเชคว่า Namespace ถูก apply แล้ว

```shell
kubectl get ns
```

2. สร้าง K8S Deployemnt ของ frontend

`01-frontend-deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  namespace: ntayz-k8s-recipe-2
  labels:
    name: frontend
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - name: frontend
          image: ntayz-k8s-recipe-2/frontend:1.0.0
          resources:
            requests:
              memory: 500Mi
              cpu: 200m
            limits:
              memory: 1Gi
              cpu: 500m
          livenessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 300
          readinessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 30
            failureThreshold: 5
          ports:
            - containerPort: 3000
              name: frontend3000
```

สร้าง frontend deployment

```shell
 kubectl apply -f ./k8s/01-frontend-deployment.yaml
```

คำสั่งเชคว่า Deployment ถูก apply แล้ว

```shell
kubectl get deployment -n ntayz-k8s-recipe-2
```

3. สร้าง K8S Services ของ frontend

`02-frontend-service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  namespace: ntayz-k8s-recipe-2
  labels:
    name: frontend
spec:
  selector:
    name: frontend
  ports:
    - name: frontend3000
      port: 3000
      targetPort: 3000
      protocol: TCP
```

สร้าง frontend Service

```shell
 kubectl apply -f ./k8s/02-frontend-service.yaml
```

คำสั่งเชคว่า Service ถูก apply แล้ว

```shell
kubectl get service -n ntayz-k8s-recipe-2
```

4. สร้าง Ingress สำหรับเชื่อมต่อ request จากนอก K8S Cluster

`99-ingress.yaml`

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myingress
  namespace: ntayz-k8s-recipe-2
  labels:
    name: myingress
spec:
  rules:
    - host: ntayz-k8s-recipe-2.io
      http:
        paths:
          - pathType: Prefix
            path: "/"
            backend:
              service:
                name: frontend
                port:
                  number: 3000
```

สร้าง ingress

```shell
 kubectl apply -f ./k8s/99-frontend-ingress.yaml
```

ลอง ingress

```shell
kubectl get ing -n ntayz-k8s-recipe-2
```

5. ลองสร้าง docker images ของ `backend1` project

```shell
docker build -f docker/Dockerfile.nestjs.prod ./backend1/ -t ntayz-k8s-recipe-2/backend1:1.0.0
```

6. สร้าง K8S Deployemnt ของ backend1

`03-backend1-deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend1
  namespace: ntayz-k8s-recipe-2
  labels:
    name: backend1
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      name: backend1
  template:
    metadata:
      labels:
        name: backend1
    spec:
      containers:
        - name: backend1
          image: ntayz-k8s-recipe-2/backend1:1.0.0
          resources:
            requests:
              memory: 500Mi
              cpu: 200m
            limits:
              memory: 1Gi
              cpu: 500m
          livenessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 300
          readinessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 30
            failureThreshold: 5
          ports:
            - containerPort: 3000
              name: backend13000
```

```shell
 kubectl apply -f ./k8s/03-backend1-deployment.yaml
```

7. สร้าง K8S Service ของ backend1

`04-backend1-service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: backend1
  namespace: ntayz-k8s-recipe-2
  labels:
    name: backend1
spec:
  selector:
    name: backend1
  ports:
    - name: backend13000
      port: 3000
      targetPort: 3000
      protocol: TCP
```

```shell
 kubectl apply -f ./k8s/04-backend1-service.yaml
```

10. สร้าง Ingress สำหรับเชื่อมต่อ backend จากนอก K8S Cluster

`100-backend-ingress.yaml`

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myingress-backend
  namespace: ntayz-k8s-recipe-2
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  labels:
    name: myingress-backend
spec:
  ingressClassName: "nginx"
  rules:
    - host: kubernetes.docker.internal
      http:
        paths:
          - pathType: Prefix
            path: /backend1(/|$)(.*)
            backend:
              service:
                name: backend1
                port:
                  number: 3000
```

```shell
 kubectl apply -f ./k8s/100-backend-ingress.yaml
```

11. ลองสร้าง docker images ของ `backend2` project

```shell
docker build -f docker/Dockerfile.nestjs.prod ./backend2/ -t ntayz-k8s-recipe-2/backend2:1.0.0
```

12. สร้าง K8S Deployemnt ของ backend2

`05-backend2-deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend2
  namespace: ntayz-k8s-recipe-2
  labels:
    name: backend2
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      name: backend2
  template:
    metadata:
      labels:
        name: backend2
    spec:
      containers:
        - name: backend2
          image: ntayz-k8s-recipe-2/backend2:1.0.1
          env:
            - name: NODE_ENV
              value: "production"
            - name: PORT
              value: "3000"
          resources:
            requests:
              memory: 500Mi
              cpu: 200m
            limits:
              memory: 1Gi
              cpu: 500m
          livenessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 300
          readinessProbe:
            tcpSocket:
              port: 3000
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 30
          #   failureThreshold: 5
          ports:
            - containerPort: 3000
              name: backend23000
```

```shell
 kubectl apply -f ./k8s/05-backend2-deployment.yaml
```

13. สร้าง K8S Service ของ backend2

`06-backend2-service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: backend2
  namespace: ntayz-k8s-recipe-2
  labels:
    name: backend2
spec:
  selector:
    name: backend2
  ports:
    - name: backend23000
      port: 3000
      targetPort: 3000
      protocol: TCP
```

```shell
 kubectl apply -f ./k8s/06-backend2-service.yaml
```

14. อัปเดต Ingress สำหรับเชื่อมต่อ backend จากนอก K8S Cluster

`100-backend-ingress.yaml`

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myingress-backend
  namespace: ntayz-k8s-recipe-2
  annotations:
    # kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  labels:
    name: myingress-backend
spec:
  ingressClassName: "nginx"
  rules:
    - host: kubernetes.docker.internal
      http:
        paths:
          - pathType: Prefix
            path: /backend1(/|$)(.*)
            backend:
              service:
                name: backend1
                port:
                  number: 3000
          - pathType: Prefix
            path: /backend2(/|$)(.*)
            backend:
              service:
                name: backend2
                port:
                  number: 3000
```

```shell
 kubectl apply -f ./k8s/100-backend-ingress.yaml
```

15. หลังจากทดลองเสร็จเสร็จแล้วืทำการเคลียร์ Namespace

```shell
kubectl delete -f 00-namespace.yaml
```
